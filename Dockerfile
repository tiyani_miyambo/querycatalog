FROM python:3.8-slim

RUN apt-get update \
&& apt-get install curl -y \
 && apt-get install unixodbc -y \
 && apt-get install unixodbc-dev -y \
 && apt-get install freetds-dev -y \
 && apt-get install freetds-bin -y \
 && apt-get install tdsodbc -y \
 && apt-get install --reinstall build-essential -y

RUN pip install --trusted-host pypi.python.org pyodbc==4.0.30 sqlalchemy==1.3.5

ADD odbcinst.ini /etc/odbcinst.ini

RUN apt-get clean -y
WORKDIR  /usr/src/querycatalog
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_PORT=5001
#Server will reload itself on file changes if in dev mode
ENV FLASK_ENV=development 
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN pip install gunicorn
COPY . .
EXPOSE 5000
CMD ["flask", "run"]
#ENTRYPOINT [ "./boot.sh" ]