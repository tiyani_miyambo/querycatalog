import pyodbc
import itertools
import json
import time

def createSQLServerConn():
    print('making connection to SQL Server Please wait ...') 
    sleep =3
    retry=3
    count=0
    success = False  

    while count < retry and success == False:
        try:
            server = '192.168.8.110,1433'
            conn =pyodbc.connect('Driver={SQL Server};' 
                                'Server='+ server + ';' 
                                'UID=BI;'
                                'PWD=tkny;'
                                'DATABASE=DWH')

            crs = conn.cursor()
            print('connection made on SQL Server')
            success =True
        except:
            count += 1
            time.sleep(sleep)
            print('Connection to SQL server failed retry ## {}'.format(count))

    if success == True:
        return {'success':True,'value': crs}
    else:
        return {'success':False,'message':'Unable to connect to SQL server {} after {} attempts'.format(server,count)}


def SQL(sqlquery):
    crsValue = createSQLServerConn()   
    if  crsValue['success'] == True:
        crs = crsValue['value']
        crs.execute(sqlquery)
        query_result = [   dict(line) for line in [zip([ column[0] for column in crs.description], row) for row in crs.fetchall()] ]
        return {'success': True, 'value' : query_result}

    else:
        return crsValue
    
    

# This function will pivot a parameter name/value/casting into a dictionary
def pivotParam (resultItems, keyCol, valueCol, colCasting=""):
    # create the items from the json block    
    #items = json.loads(resultItems)
    items = resultItems
    # for row in items:
    outputItem = {}
    for row in items:
        paramName = row[keyCol]
        
        if colCasting == "":
            paramValue = row[valueCol]
        else:
            dataType = row[colCasting]
            if dataType == "str":
                paramValue = str(row[valueCol])
            elif dataType == "bool":
                paramValue = bool(row[valueCol])
            elif dataType == "int":
                paramValue = int(row[valueCol]) 
        # Append the values received
        outputItem[paramName]=paramValue
    return outputItem






