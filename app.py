from flask import Flask,request
app = Flask(__name__)
from querycatalog import Query
from flask import jsonify

@app.route('/')
def Index():
    html = """\
    <html> 
    <head>
        <style>
            @media all {body{text-align:left;} p.paragrpahs
            {text-align:center;font-family:Arial,Helvetica,sans-serif;font-size:32px;font-weight:bold;vertical-align: center;}.break_here
            {page-break-before:always;}}
        </style>
    </head><body><br/><br/><br/> <p class="break_here"><p class="paragrpahs">Query Catalog Microservice Page</p><p class="break_here">
    <br/><br/><p class="break_here"><p class="paragrpahs">This API support GET requests for microservices, "/api/v1.0/{endpoint}" </p><p class="break_here"><br/> 
    </body>
    </html>"""
    return (html)

@app.route('/api/v1.0/ParamValues/<string:InputName>')
def queryParamValues(InputName):
    sqlquery = """ Select ParamName,
                          ParamValue
                    From dbo.vProcessParam
                    Where ProcessName = '{}'""".format(InputName)

    results = (Query.SQL(sqlquery))
    if results['success'] == False:
        return results
    else:
        results = results['value']
        pivotResults = Query.pivotParam(results,"ParamName","ParamValue")               
        return jsonify(pivotResults)

@app.route('/api/v1.0/EmailValues/<string:MsgType>/<string:ProcessName>')
def queryEmailValues(MsgType,ProcessName):
    sqlquery = """ SELECT
                        UserName,
                        FirstName,
                        LastName,
                        MessageCategory,
                        MessageText
                   From dbo.vEmailProcess
                   Where ProcessName = '{}' and MessageName = '{}' """.format(ProcessName,MsgType)
    results =(Query.SQL(sqlquery))
    if results['success'] == False:
        return results
        
    else:
        results = results['value']    
        results = results[0]
        print(type(results))
        return jsonify(results)

@app.route('/api/v1.0/FixedRules/<string:ProcessName>')
def fixedfilerules(ProcessName):
    sqlquery = """ SELECT 
                     ParamColumnName
                    ,ParamMin
                    ,ParamMax
                 From dbo.vProcessFixedFileRules
                 Where ProcessName = '{}'
                 order by ParamOrder """.format(ProcessName)
    results =(Query.SQL(sqlquery))
    if results['success'] == False:
        return results
        
    else:
        results = results['value']    
        print(type(results))
        return jsonify(results)

@app.route('/api/v1.0/FileInfo/<string:FileName>')
def FileInfo(FileName):
    sqlquery = """ select 
                        ProcessName,
                        FileType
                    from dbo.vFileInfo
                    where FileName = '{}' """.format(FileName)
    results =(Query.SQL(sqlquery))
    if results['success'] == False:
        return results        
    else:
        results = results['value']    
        print(type(results))
        return jsonify(results)



    

